#!/usr/bin/python3
from IMA import docker
from abc import ABC, abstractmethod

# the abstract class adaptors has all slice methods that will be implement by each VIM adaptor
class Adaptors(ABC):
   
    @abstractmethod
    def create_slice(self):
        pass

    @abstractmethod
    def remove_slice(self):
        pass

    @abstractmethod
    def slice_service_scaling(self):
        pass

    @staticmethod
    def select_adaptor(adaptor_key):
        available_adaptors = {'swarm': 1, 'kubernetes': 2, 'openstack': 3}
        if adaptor_key in available_adaptors:
            if available_adaptors.get(adaptor_key) == 1:
                adaptor = SwarmAdaptor()
                return adaptor

class SwarmAdaptor(Adaptors):
    #initialize docker client, stack and service variables
    def __init__(self):
        self.docker_client   = docker.DockerClient()
        self.docker_stack    = docker.DockerStack()
        self.docker_services = docker.DockerServices()

    # adaptor method call docker stack create command (overriding abstract method)
    def create_slice(self, slice_description, stack_service_name):
        self.docker_stack.create_stack(slice_description, stack_service_name)
    
    # adaptor method call docker stack remove command (overriding abstract method)
    def remove_slice(self, slice_name):
        self.docker_stack.remove_stack(slice_name)
    
    # adaptor method call docker servcie scale command (overriding abstract method)
    def slice_service_scaling(self, service_name, instances):
        self.docker_services.service_scaling(service_name, instances)

    def get_docker_client(self):
        return self.docker_client



