#!/usr/bin/python3
import os, sys, docker, time

# class to connect docker API in swarm leader server
class DockerClient:
    def __init__(self):
        self.client = docker.from_env()
    
    def get_client():
        return self.client 

# class to manage docker stack commands to deploy slice with services
class DockerStack:
    # create docker stack
    def create_stack(self, slice_description, stack_service_name):
        os.system("docker stack deploy -c " + slice_description + " " + stack_service_name)
    
    # remove docker stack
    def remove_stack(self, slice_name):
        os.system("docker stack rm " + slice_name)

# class to manage docker services already crated in docker deploy
class DockerServices:
    # realize scaling up and down based in service name and instances params 
    def service_scaling(self, service_name, instances):
        os.system("docker service scale " + service_name + "=" + str(instances))
        time.sleep(5)
