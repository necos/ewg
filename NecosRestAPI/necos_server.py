#!/usr/bin/python3

import os, sys
from flask import Flask, request, jsonify, make_response, abort
from flask_request_params import bind_request_params
sys.path.append('../')
from SliceBuilder import slice_builder
from SliceResourceOrchestrator import SRO

# flask variables
app = Flask(__name__)
app.before_request(bind_request_params)

#slice builder variables
slice_builder = slice_builder.SliceBuilder()

#sro variables
sro = SRO.InteligentRunTimeManagement()

# custom error message
@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)

        
# create RESt API GET method for slices
@app.route('/slices', methods=['GET'])
def get_slices():    
    SLICES = slice_builder.get_slices()
    return jsonify(SLICES)


# create RESt API POST method for slices
@app.route('/slices', methods=['POST'])
def create_slice():
    request_file = request.files['filedata']
    file_name = request_file.filename
    if not request_file:
        return "No file"

    # get file content 
    file_contents = request_file.stream.read().decode("utf-8")
    
    # write a new file 
    f = open(file_name, "w")
    f.write(file_contents)
    f.close()
    
    # call slice cration method from SRO and pass file_name (slice description) for deployment
    created_slice = slice_builder.create_slice(file_name)
    
    #return created slice
    return jsonify(created_slice), 201
   
# create RESt API GET method for get slice by id
@app.route('/slices/<int:slice_id>', methods=['GET'])
def get_slice(slice_id):
    slice_search = slice_builder.get_slice(slice_id)
    try:
        slice_search = slice_search['slice']
        return jsonify({'slice': slice_search})
    except KeyError:
        return abort(404)
             
# create RESt API DELETE method for get slice by id
@app.route('/slices/<int:slice_id>', methods=['DELETE'])
def delete_slice(slice_id):
    result = slice_builder.remove_slice(slice_id)
    if result == 200:
        return jsonify({'result': True})
    else:
        return abort(404)


# create RESt API PUT method for elasticity (upgrade) in specific service in slice
@app.route('/slices/<int:slice_id>/elasticity/upgrade', methods=['PUT'])
def elasticity_upgrade(slice_id):
    parameters = request.params.require('service').permit('name')
    service_name = parameters['name']
    result = sro.elasticity_upgrade(slice_id, service_name)
    if result == 200:
        return jsonify({'result': True})
    else:
        return abort(404)

# create RESt API PUT method for elasticity (downgrade) in specific service in slice
@app.route('/slices/<int:slice_id>/elasticity/downgrade', methods=['PUT'])
def elasticity_downgrade(slice_id):
    parameters = request.params.require('service').permit('name')
    service_name = parameters['name']
    result = sro.elasticity_downgrade(slice_id, service_name)
    if result == 200:
        return jsonify({'result': True})
    else:
        return abort(404)


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')