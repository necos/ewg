# Slice Resource Orchestrator and Slices Database components

This repository describes two prototype components of NECOS: the **Slice Resource Orchestrator** and the **Slices Database**.
The Slice Resource Orchestrator component binds the slice requirements to build a slice. The Slices Database component stores and retrieves data about slices using Neo4j, a graph database.
The Slices Database prototype has been implemented in order to interact with the current SRO implementation. The interfaces implement the methods agreed by the EWG and the next step is to extend the internal structure to meet the requirements of the NECOS Information Model. Also, some parts of the current SRO have been modified to interact with the Slices Database prototype.

![Components and interfaces](https://www.tamps.cinvestav.mx/~faguirre/dev/sro-img/fig01.components.png)

## Testbed
Our testbed consists in two virtual machines (VM1 and VM2) that conform a cluster, in this case we are using Docker Swarm with VM1 as the leader of the swarm.
The "SRO+Slice Builder" and the "Slices Database" components, as well as the Neo4j database are running on VM1, serving on different ports. This testbed can be seen in Fig. 1.

![Testbed description](https://www.tamps.cinvestav.mx/~faguirre/dev/sro-img/fig01.testbed.png)

## Installation:

### Prerequirements

First, you need to install:
* Docker-ce
* Python3
* Pip3
* Neo4j

*For a step by step installation read [this document](https://www.tamps.cinvestav.mx/~faguirre/dev/sro-img/Testbed.pdf)*.

After that, install pip3 modules:

-------------
    # pip3 install -r requirements.txt


### Cluster initialization:

In VM1, create cluster leader:

-------------
    # docker swarm init --advertise-addr=VM1_IP

In VM2, join to cluster:

-------------
    # docker swarm join --token xxxx VM1_IP:2377


### APIs initialization:

To run the Slices Database API:

-------------
    # python3 SlicesDatabase/dbserver.py

To run the Slice Resource Orchestrator API:

-------------
    # python3 SliceResourceOrchestrator/server.py

### Consuming the APIs:

#### Create slice:
To crate slice you need to pass a template. In this example, file.yml is the yml file passed. 

-------------
    # curl -i -X POST -F filedata=@file.yml http://VM1_IP:5000/slices

The SRO component binds the requirements, calls the *store_database* interface on the Slices Database API to store the information of the slice and finally builds the slice.

#### Get all the slices:
To get a list of slices just request:

-------------
    # curl -i http://VM1_IP:5000/slices

The SRO component has to make a call to the *get_slices* interface on the Slices Databases API. To call directly to the Slices Database API use (just change the port):

-------------
    # curl -i http://VM1_IP:5001/slices

#### Get a specific slice:
To get a slice, you need to pass a *slice_id* in the request:

-------------
    # curl -i http://VM1_IP:5000/slices/slice_id

The SRO component has to make a call to the *query_slice* interface on the Slices Databases API. To call directly to the Slices Database API use (just change the port):

-------------
    # curl -i http://VM1_IP:5001/slices/slice_id

#### Elasticity (ugrade):
To apply elasticity in a specific slice, you need to pass *slice_id* and *service_name*. In this example, *service_name* is "test". 

-------------
    # curl -i -X PUT http://VM1_IP:5000/slices/slice_id/elasticity/upgrade -d 'service[name]=test'

The SRO component calls the *query_slice* interface on the Slices Database API to get the instances of the given service. Then it increments by one the instances on the cluster and updates the slice information by calling the *update_slice* interface on the Slices Database API.

#### Elasticity (downgrade):
To apply elasticity in a specific slice, you need to pass *slice_id* and *service_name*. In this example, *service_name* is "test". 

-------------
    # curl -i -X PUT http://VM1_IP:5000/slices/slice_id/elasticity/downgrade -d 'service[name]=test'

The SRO component calls the *query_slice* interface on the Slices Database API to get the instances of the given service. Then it decrements by one the instances on the cluster and updates the slice information by calling the *update_slice* interface on the Slices Database API.

#### Delete slice:
To delete a slice you need to pass the *slice_id* in the request:

-------------
    # curl -i -X DELETE http://VM1_IP:5000/slices/slice_id

The SRO calls the *query_slice* interface on the Slices Database API to get the slice to remove, the it calls the *delete_slice* interface on the Slices Database API to remove the information of the slice and finally removes the slice on the cluster.

### Graph view
A created slice can be seen in Neo4j Browser as a graph whith these relationships:

![Graph view of a slice](https://www.tamps.cinvestav.mx/~faguirre/dev/sro-img/fig.2.graph.png)

This graph has the following nodes and properties:

![Nodes and properties](https://www.tamps.cinvestav.mx/~faguirre/dev/sro-img/fig.3.text.png)