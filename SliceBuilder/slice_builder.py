
#!/usr/bin/python3
import os, sys, docker, yaml, time, json, requests
sys.path.append('../')
from IMA import VIM_adaptors

class SliceBuilder:
    def __init__(self):
        self.SLICES_DATABASE_URL = 'http://localhost:5001/slices'
        self.json_header = {'Content-Type': 'application/json'}
    
        

    # return slices list
    def get_slices(self):
        r = requests.get(self.SLICES_DATABASE_URL)
        return r.json()
    
    # get slice by id
    def get_slice(self, slice_id):
        r = requests.get('{}/{}'.format(self.SLICES_DATABASE_URL, slice_id))
        return r.json()

    # get data from slice description and create slice specification in SLICES list        
    def create_slice_specification(self, slice_description):
        with open(slice_description, 'r') as stream:
            # temporary variables
            data_loaded = yaml.load(stream)
            tenant = ""
            slice_name = "" 
            VIM = ""
            service = ""
            image = ""
            network  = []
            instances = 1
            ports = ""

            for key, value in data_loaded.items():
                if key == "x-tenant":
                    tenant = value
                elif key == "x-slice":
                    slice_name = value
                elif key == "x-vim":
                    VIM = value
                elif key == "services":
                    services = []
                    for service_key, service_value in value.items():
                        service_name = slice_name + "_" + service_key
                        for option_key, option_value in service_value.items():
                            if option_key == "image":
                                image = option_value
                            if option_key == "networks":
                                network = option_value[0]
                            if option_key == "ports":
                                ports = option_value

                        service = {"service_name": service_name, "image":image, "network": network, "instances":instances, "ports":ports}
                        services.append(service)
                    
                    new_slice = {
                        "slice_name": slice_name,
                        "tenant": tenant,
                        "VIM": VIM,
                        "services": services
                    }
                    print(new_slice)
                    r = requests.post(self.SLICES_DATABASE_URL, json = new_slice, headers = self.json_header)
                    return r.json(), new_slice
                    
                        
    # crate slice deploymend base on slice description
    def create_slice(self, slice_description):
        response, slice_descriptor = self.create_slice_specification(slice_description)
        # the slice is always deployd at the end SLICE list, we just have to get the last position and then deploy in docker...
        stored_slice = self.get_slice(response['slice_id'])
        stack_service_name = slice_descriptor['slice_name']
        adaptor_key = slice_descriptor['VIM']         
        adaptor = VIM_adaptors.Adaptors.select_adaptor(adaptor_key)
        # deploy slice parts in DCs specified in slice description template
        adaptor.create_slice(slice_description, stack_service_name)
        return stored_slice
        
    # remove slice parts deployment in DCs specified in slice description template
    def remove_slice(self, slice_id):
        slice_instance = self.get_slice(slice_id)
        try:
            slice_data = slice_instance['slice']
            requests.delete('{}/{}'.format(self.SLICES_DATABASE_URL, slice_id))
            slice_name = slice_data['slice_name']
            adaptor_key = slice_data['VIM']         
            adaptor = VIM_adaptors.Adaptors.select_adaptor(adaptor_key)
            adaptor.remove_slice(slice_name)
            return 200
        except KeyError:
            return 404

