#!/usr/bin/python3
import os, sys, docker, yaml, time, json, requests
sys.path.append('../')
from IMA import VIM_adaptors
from SliceBuilder import slice_builder

# Slice Resource Orchestrator Core
class SROCore:

    @staticmethod
    def forward_VIM_WIM_pointers():
        pass

    @staticmethod
    def update_slice_pointers():
        pass

    @staticmethod
    def update_slice_topology():
        pass

# Slice Management
class SliceInstanceManagement:
    @staticmethod
    def binding_finished():
        pass

    @staticmethod
    def confirm_slice_decommission(slice):
        pass

    @staticmethod
    def inactivate_slice_topology(slice_topology):
        pass

    @staticmethod
    def receive_full_slice_details(slice_details):
        pass

    @staticmethod
    def recover_slice_topology(slice_id):
        pass

    @staticmethod
    def remove_slice_pointersIM(slice, pointers):
        pass

    @staticmethod
    def request_e2e_parts_binding(slice, parts):
        pass

    @staticmethod
    def request_slice_parts_decomission(slice, parts):
        pass

    @staticmethod
    def store_full_slice_details(slice_details):
        pass


# Decision Making
class InteligentRunTimeManagement:
    def __init__(self):
        self.SLICES_DATABASE_URL = 'http://localhost:5001/slices'
        self.json_header = {'Content-Type': 'application/json'}
      
        
    
    # return service running instance in specific slice
    def get_service_instances(self, slice_id, service_name):
        r = requests.get('{}/{}'.format(self.SLICES_DATABASE_URL, slice_id))
        try:
            slice_data = r.json()['slice']
            services_list = slice_data['services']
            service = [service for service in services_list if service['service_name'] == service_name]
            if len(service) == 0:
                print("service " + service_name + " not exist!")
                return 404, 0
            else:
                instances = service[0]['instances']
                return 200, instances
        except KeyError:
            return 404, 0
            

    # update service running instance in specific slice
    def set_service_instances(self, slice_id, service_name, new_instance):
        r = requests.get('{}/{}'.format(self.SLICES_DATABASE_URL, slice_id))
        slice_data = r.json()['slice']
        services_list = slice_data['services']
        service = [service for service in services_list if service['service_name'] == service_name]
        instance = {'instances': new_instance}
        service[0].update(instance)
        requests.put('{}/{}'.format(self.SLICES_DATABASE_URL, slice_id), json=slice_data, headers=self.json_header)
        print("service " + service_name + " updated!")


    # apply horizontal (upgrade) elasticity for service in slice
    def elasticity_upgrade(self, slice_id, service_name):
        result, instances = self.get_service_instances(slice_id, service_name)
        if result == 404:
            return 404
        else: 
            r = requests.get('{}/{}'.format(self.SLICES_DATABASE_URL, slice_id))
            try:    
                instances+=1
                self.set_service_instances(slice_id, service_name, instances)
            
                slice_data = r.json()['slice']
                
                adaptor_key = slice_data['VIM']      
                adaptor = VIM_adaptors.Adaptors.select_adaptor(adaptor_key)
                adaptor.slice_service_scaling(service_name, instances)
                return 200
            except KeyError:
                return 404

    # apply horizontal (downgrade) elasticity for service in slice
    def elasticity_downgrade(self, slice_id, service_name):
        result, instances = self.get_service_instances(slice_id, service_name)
        if result == 404:
            return 404
        else: 
            r = requests.get('{}/{}'.format(self.SLICES_DATABASE_URL, slice_id))
            try:    
                instances-=1
                self.set_service_instances(slice_id, service_name, instances)
            
                slice_data = r.json()['slice']
                
                adaptor_key = slice_data['VIM']      
                adaptor = VIM_adaptors.Adaptors.select_adaptor(adaptor_key)
                adaptor.slice_service_scaling(service_name, instances)
                return 200
            except KeyError:
                return 404
    

    @staticmethod
    def slice_monitoring_data(slice_details):
        pass
    
    @staticmethod
    def horizontal_elasticity(slice_details):
        pass
    
    @staticmethod
    def vertical_elasticity(slice_details):
        pass