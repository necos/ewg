#!/usr/bin/python3

from neo4j import GraphDatabase
import os, sys, yaml, time, json

db_uri = "bolt://localhost:7687"
db_auth = ("neo4j", "admin")
driver = GraphDatabase.driver(db_uri, auth=db_auth)

def addService(tx, service):
    result = tx.run("MERGE (i:Image {image_name: $image_name}) "
           "MERGE (n:Network {network_name: $network_name}) "
           "CREATE (s:Service {service_name: $service_name, instances: $instances}) "
           "CREATE (s)-[:CONNECTED_TO]->(n)"
           "CREATE (s)-[:IS_BASED_ON]->(i)"
           "RETURN id(s)",
           service_name=service['service_name'], instances=service['instances'],
           network_name=service['network'], image_name=service['image'])
    service_id = result.single().value()
    for port in service['ports']:
        host_port, vm_port = port.split(':')
        tx.run("CREATE (p:Port {host_port: $host_port, vm_port: $vm_port}) "
               "WITH p "
               "MATCH (s:Service) WHERE id(s)={id} "
               "CREATE (s)-[:EXPOSES]->(p)",
               host_port=int(host_port), vm_port=int(vm_port), id=service_id)
    return service_id

def addSlice(tx, slice_name, tenant, VIM):
    result = tx.run("CREATE (s:Slice {slice_name: $slice_name}) "
           "MERGE (t:Tenant {tenant: $tenant}) "
           "MERGE (v:VIM {VIM: $VIM}) "
           "CREATE (t)-[:DEPLOYS]->(s)"
           "CREATE (v)-[:DEPLOYS]->(s)"
           "RETURN id(s)",
           slice_name=slice_name, tenant=tenant, VIM=VIM)
    slice_id = result.single().value()
    return slice_id

def getSlice(tx, slice_id):
    sl_res = tx.run(
        "MATCH (sl:Slice) WHERE id(sl)={slice_id} "
        "MATCH (t)-[:DEPLOYS]->(sl) "
        "MATCH (v)-[:DEPLOYS]->(sl) "
        "RETURN id(sl), sl.slice_name, t.tenant, v.VIM",
        slice_id=slice_id
    )
    sr_res = tx.run(
        "MATCH (sl:Slice) WHERE id(sl)={slice_id} "
        "MATCH (sl)-[:RUNS]->(sr:Service) "
        "MATCH (sr)-[:CONNECTED_TO]->(net:Network) "
        "MATCH (sr)-[:IS_BASED_ON]->(i:Image) "
        "MATCH (sr)-[:EXPOSES]->(p:Port) "
        "RETURN sr.service_name, sr.instances, net.network_name, i.image_name, p.host_port, p.vm_port",
        slice_id=slice_id 
    )
    try:
        sl_res = sl_res.data()[0]
        sr_res = sr_res.data()
    except IndexError:
        return 404
    response = {'id': sl_res['id(sl)'], 'slice_name': sl_res['sl.slice_name'],
                'tenant': sl_res['t.tenant'], 'VIM': sl_res['v.VIM'], 'services': []}
    for sr in sr_res:
        ports = [str(sr['p.host_port']) + ':' + str(sr['p.vm_port'])]
        service = {'service_name': sr['sr.service_name'], 'instances': sr['sr.instances'],
                    'network': sr['net.network_name'], 'image': sr['i.image_name'], 'ports': ports}
        response['services'].append(service)
    return response

def delete_database():
    global driver
    with driver.session() as session:
        result = session.run("MATCH (n) DETACH DELETE n")
    if result.single() == None:
        return 200
    else:
        return 500

# get data from slice description and create slice specification in SLICES list        
def store_slice(slice_descriptor):
    global driver
    with driver.session() as session:
        slice_id = session.write_transaction(addSlice, slice_descriptor['slice_name'], slice_descriptor['tenant'], slice_descriptor['VIM'])
    for service in slice_descriptor['services']:
        with driver.session() as session:
            service_id = session.write_transaction(addService, service)
        with driver.session() as session:
            session.run("MATCH (sl:Slice), (sr:Service) WHERE id(sl) = {slice_id} AND id(sr) = {service_id} "
               "CREATE (sl)-[:RUNS]->(sr)", slice_id=slice_id, service_id=service_id)
    return 201, slice_id

def query_slice(slice_id):
    global driver
    with driver.session() as session:
        slice_details = session.read_transaction(getSlice, slice_id)
    if slice_details == 404:
        return 404, 404
    return 200, slice_details

def get_slices():
    global driver
    slices = []
    with driver.session() as session:
        result = session.run("MATCH (s:Slice) RETURN id(s)")
    ids = result.value()
    for slice_id in ids:
        slices.append(query_slice(slice_id)[1])
    return 200, slices

def update_slice(slice_id, slice_descriptor):
    global driver
    for service in slice_descriptor['services']:
        with driver.session() as session:
            result = session.run("MATCH (sl:Slice) WHERE id(sl) = {slice_id} "
            "MATCH (sl)-[:RUNS]->(sr:Service) WHERE sr.service_name={service_name} "
            "RETURN sr.instances", slice_id=slice_id, service_name=service['service_name'])
        instances = result.value()[0]
        if instances != service['instances']:
            with driver.session() as session:
                result = session.run("MATCH (sl:Slice) WHERE id(sl) = {slice_id} "
                "MATCH (sl)-[:RUNS]->(sr:Service) WHERE sr.service_name={service_name} "
                "SET sr.instances={instances}", slice_id=slice_id, service_name=service['service_name'], instances=service['instances'])
    return 200

def delete_slice(slice_id):
    global driver
    with driver.session() as session:
        result = session.run(
            "MATCH (sl:Slice) WHERE id(sl) = {slice_id} "
            "MATCH (sl)-[:RUNS]->(sr:Service) "
            "MATCH (sr)-[:EXPOSES]->(p:Port) "
            "DETACH DELETE p, sr, sl",
            slice_id=slice_id
        )
    if result.single() == None:
        return 200
    else:
        return 500