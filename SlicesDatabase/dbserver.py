#!/usr/bin/python3

import os, sys
import dbfunctions as db
from flask import Flask, request, jsonify, make_response, abort
from flask_request_params import bind_request_params

# flask variables
app = Flask(__name__)
app.before_request(bind_request_params)

@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)

@app.errorhandler(500)
def internal_server_error(error):
    return make_response(jsonify({'error': 'Internal server error'}), 500)


@app.route('/slices', methods=['POST'])
def store_slice():
    slice_descriptor = request.get_json()
    result, slice_id = db.store_slice(slice_descriptor)
    if result == 201:
        return jsonify({'slice_id': slice_id}), 201
    else:
        return abort(500)


@app.route('/slices/<int:slice_id>', methods=['GET'])
def query_slice(slice_id):
    result, slice_details = db.query_slice(slice_id)
    if result == 200:
        return jsonify({'slice': slice_details})
    else:
        return abort(404)


@app.route('/slices', methods=['GET'])
def get_slices():    
    result, slices = db.get_slices()
    if result == 200:
        return jsonify({'slices' : slices})
    else:
        return abort(404)

@app.route('/slices/<int:slice_id>', methods=['DELETE'])
def delete_slice(slice_id):
    result = db.delete_slice(slice_id)
    if result == 200:
        return jsonify({'result': True})
    else:
        return abort(404)


@app.route('/slices/delete', methods=['GET'])
def delete_database():
    result = db.delete_database()
    if result == 200:
        return jsonify({'result': 'OK'}), 200
    else:
        return abort(500)


@app.route('/slices/<int:slice_id>', methods=['PUT'])
def update_slice(slice_id):
    slice_descriptor = request.get_json()
    result = db.update_slice(slice_id, slice_descriptor)
    if result == 200:
        return jsonify({'result': True})
    else:
        return abort(404)

   
if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5001)